#include <stdlib.h>
#include <stdio.h>

#include <locale.h>

int **getBuffer(int size) {
	int **buffer = (int **) calloc(sizeof(int *), size);
	for (int i = 0; i < size; i++) {
		buffer[i] = (int *) calloc(sizeof(int), size);
	}
	return buffer;
}

void clearTmp(int **src, int size) {
	for (int i = 0; i < size; i++) {
		free(src[i]);
	}
	free(src);
}

void extractMatrix(int **src, int **target, int r, int c, int size) {
	int row = 0, col = 0;
	for (int i = 0; i < size; i++) {
		if (i == r) {
			continue;
		} else {
			for (int j = 0; j < size; j++) {
				if (j == c) continue;
				target[row][col++] = src[i][j];
			}
			row++;
		}
	}
}

int getDeterminant(int **mat, int size) {
	if (size == 2) {
		return mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
	} else {
		int **minor = getBuffer(size - 1);
		int res = 0;
		for (int c = 0, sign = 1; c < size; sign *= -1, c++) {
			extractMatrix(mat, minor, 0, c, size);
			res += sign * mat[0][c] * getDeterminant(minor, size - 1);
		}
		clearTmp(minor, size);
		return res;
	}
}


int main() {
	setlocale(LC_ALL,"Russian");
	printf("������� ������");
	int size;
	scanf("%d",&size);
	int **mat = getBuffer(size);
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			int tmp;
			scanf("%d",&tmp);
			mat[i][j] = tmp;
		}
	}
	printf("%d\n",getDeterminant(mat, size));

	clearTmp(mat, size);
	return 0;
}
